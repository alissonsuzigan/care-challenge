import { customFetch } from './utils';

// API contants
const URL = 'https://api.openweathermap.org/data/2.5/weather';
const KEY = '?APPID=24312b65699c3bccbcd0fab6eeaf5681';
const UNIT = '&units=metric';
const QUERY = '&q=';

/**
 * Get weather data by city
 * @param {String} city
 * @return {Promise}
 */
const getWeatherByCity = (city) => {
  if (!city) throw Error('The "city" param was not passed!');
  const url = `${URL}${KEY}${UNIT}${QUERY}${city}`;
  return customFetch(url);
};

export default getWeatherByCity;