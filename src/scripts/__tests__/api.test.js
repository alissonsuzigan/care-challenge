import getWeatherByCity from '../api';

describe('Validate getWeatherByCity method', () => {
  it('Should throw an error when no city has been passed', () => {
    expect(() => getWeatherByCity()).toThrow(Error);
  });

  it('Should return an object with the prop name equals to Berlin', async () => {
    const result = await getWeatherByCity('berlin');
    expect(result).toBeInstanceOf(Object);
    expect(result.name).toBe('Berlin');
  });
});
