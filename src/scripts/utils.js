/**
 * An abstraction to the fetch method
 * @param {String} url
 * @param {Object} params
 * @return {Promise}
 */
export const customFetch = async (url, params = {}) => {
  const response = await fetch(url, params);
  if (response.ok) return response.json();
  return Promise.reject(new Error(response.status));
};