export default {
  "coord": {
    "lon": -0.1,
    "lat": 53.52
  },
  "weather": [
    {
      "id": 803,
      "main": "Clouds",
      "description": "broken clouds",
      "icon": "04n"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 3.14,
    "pressure": 1043,
    "humidity": 93,
    "temp_min": 3,
    "temp_max": 4
  },
  "visibility": 10000,
  "wind": {
    "speed": 1.5,
    "deg": 270
  },
  "clouds": {
    "all": 76
  },
  "dt": 1546474800,
  "sys": {
    "type": 1,
    "id": 1422,
    "message": 0.0044,
    "country": "GB",
    "sunrise": 1546503373,
    "sunset": 1546530814
  },
  "id": 2634844,
  "name": "Waltham",
  "cod": 200
};