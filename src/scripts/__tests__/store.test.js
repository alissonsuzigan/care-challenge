import { showLoading, loadWeatherByCity } from '../store';

describe('Verify action creator methods', () => {
  it('Should validate showLoading method', () => {
    const action = showLoading('moscow');
    expect(action).toEqual({
      type: 'LOAD_WEATHER_REQUEST',
      payload: { city: 'moscow' }
    });
  });

  it('Should validate loadWeatherByCity success', async() => {
    const action = await loadWeatherByCity('berlin');
    expect(action.type).toBe('LOAD_WEATHER_SUCCESS');
    expect(action.payload.city).toBe('berlin');
    expect(action.payload.data.main).toBeInstanceOf(Object);
    expect(action.payload.data.weather).toBeInstanceOf(Array);
  });

  it('Should validate loadWeatherByCity error', async () => {
    const action = await loadWeatherByCity('asdasdasd');
    expect(action).toEqual({
      type: 'LOAD_WEATHER_ERROR',
      payload: { city: 'asdasdasd' }
    });
  });
});
