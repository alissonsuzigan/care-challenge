import React from 'react';
import { mount } from 'enzyme';
import Search from './search';
import { INITIAL_STATE } from '../../scripts/store.js';


describe('Search component', () => {
  it('Should verify the component with modal closed', () => {
    const wrapper = mount(<Search state={INITIAL_STATE} />);
    expect(wrapper.find('div.search-content')).toBeDefined();
    expect(wrapper.find('form.form')).toBeDefined();
    expect(wrapper.find('button.fixed-button')).toBeDefined();
  });

  it('Should render modal when the fixed button is clicked', () => {
    const wrapper = mount(<Search state={INITIAL_STATE} />);
    const button = wrapper.find('button.fixed-button');
    expect(wrapper.find('div.modal')).toHaveLength(0);
    button.simulate('click');
    expect(wrapper.find('div.modal')).toHaveLength(1);
  });

  it('Should search a new city', () => {
    const wrapper = mount(<Search state={INITIAL_STATE} />);
    wrapper.find('button.fixed-button').simulate('click');
    expect(wrapper.find('div.modal')).toHaveLength(1);
    wrapper.find('input.input').simulate('change', { target: { value: 'Moscow' } });
    // wrapper.find('button.submit').simulate('click');
    wrapper.find('form.form').simulate('submit');

    setTimeout(() => {
      expect(wrapper.find('span.feedback').text()).toBe('Loading....');
      jest.runAllTimers();
    }, 100);
  });
});
