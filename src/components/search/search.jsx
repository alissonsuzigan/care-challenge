import React, { useState, useContext, useRef } from 'react';
import { MdSearch, MdClose } from "react-icons/md";
import WeatherContext, { showLoading, loadWeatherByCity } from '../../scripts/store';

const Search = ({ state }) => {
  const { dispatch } = useContext(WeatherContext);
  const [city, setCity] = useState('');
  const [showModal, setShowModal] = useState(false);
  const inputEl = useRef(null);

  const loadWeather = async (term) => {
    if (term && term !== state.city) {
      dispatch(showLoading(term));
      const payload = await loadWeatherByCity(term);
      dispatch(payload);

      if (payload.type === 'LOAD_WEATHER_SUCCESS') {
        setShowModal(false);
      }
    }
  };

  const onChangeInput = (event) => {
    setCity(event.target.value);
  };

  const onToggleModal = () => {
    setShowModal(prev => {
      if (!prev) {
        setTimeout(() => {
          setCity('');
          inputEl.current.focus();
        }, 0);
      }
      return !prev;
    });
  };

  const submitWeather = (event) => {
    event.preventDefault();
    loadWeather(city);
  };

  return (
    <div className="search-content">
      <button className="fixed-button" onClick={onToggleModal} title="Open search form"><MdSearch /></button>
      {showModal &&
        <div className="modal">
          <form onSubmit={submitWeather} className="form">
            <button className="close" title="Close search form"><MdClose size={24} onClick={onToggleModal} /></button>
            <label className="label" htmlFor="locale">City:</label>
            <input className="input" ref={inputEl} id="locale" value={city} onChange={onChangeInput} placeholder="Search for a city" />
            <button className="submit" title="Search now"><MdSearch size={24} /></button>
            <span className="feedback">{state.feedback} </span>
          </form>
        </div>
      }
    </div>
  );
};

export default Search;
