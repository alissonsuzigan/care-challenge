export default {
  "coord": {
    "lon": 13.39,
    "lat": 52.52
  },
  "weather": [
    {
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01n"
    }
  ],
  "base": "stations",
  "main": {
    "temp": -1.51,
    "pressure": 1031,
    "humidity": 68,
    "temp_min": -2,
    "temp_max": -1
  },
  "visibility": 10000,
  "wind": {
    "speed": 3.1,
    "deg": 300
  },
  "clouds": {
    "all": 0
  },
  "dt": 1546474800,
  "sys": {
    "type": 1,
    "id": 1275,
    "message": 0.0559,
    "country": "DE",
    "sunrise": 1546499816,
    "sunset": 1546527894
  },
  "id": 2950159,
  "name": "Berlin",
  "cod": 200
};