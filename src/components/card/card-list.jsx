import React from 'react';
import Card from './card';

const CardList = ({ data }) => {
  const cityList = [];

  for (const key in data) {
    cityList.unshift(data[key]);
  }

  return (
    <div className="card-list">
      {cityList.map(city => <Card key={city.name} data={city} />)}
    </div>
  );
};

export default CardList;