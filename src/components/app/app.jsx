import React, { useEffect } from 'react';
import CardList from '../card';
import Search from '../search';
import WeatherContext, { showLoading, loadWeatherByCity, userWeatherReducer } from '../../scripts/store';

const App = () => {
  const [state, dispatch] = userWeatherReducer();
  const cityList = ['waltham', 'berlin'];

  const loadWeatherByCityList = async () => {
    for (const city of cityList) {
      dispatch(showLoading(city));
      const data = await loadWeatherByCity(city);
      dispatch(data);
    }
  };

  useEffect(() => {
    loadWeatherByCityList();
  }, []);

  return (
    <WeatherContext.Provider value={{ dispatch }}>
      <CardList data={state.data} />
      <Search state={state} />
    </WeatherContext.Provider>
  );
};

export default App;