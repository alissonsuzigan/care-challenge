# Code Challenge :: Weather

The application displays by default the weather from Berlin and Waltham, and also allows the user search for other cities.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
```
node > 8.0.0
npm > 5.0.0
```

### Installing and running the application

* Cloning the repository:
```
git clone git@bitbucket.org:alissonsuzigan/care-challenge.git
```

* Installing the project dependencies:
```
npm install
```

* **IMPORTANT:** It is necessary to install the alpha versions of the React and ReactDom, because this app uses the new hooks features:
```
npm install react@16.7.0-alpha.2 react-dom@16.7.0-alpha.2
```

* Runing the application:
```
npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### Running the tests

* Launching the test runner in watch mode:
```
npm test
```

* Generating the code coverage:
```
npm run test:cover
```

### Deployment

* Building the application:
```
npm run build
```

This command builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
