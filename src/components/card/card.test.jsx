import React from 'react';
import { mount } from 'enzyme';
import CardList from './card-list';
import Card from './card';
import berlinPayload from '../../__mocks__/berlin-payload';
import walthamPayload from '../../__mocks__/waltham-payload';

const mockData = {
  berlin: berlinPayload,
  waltham: walthamPayload
}

describe('CardList component', () => {
  it('Should render CardList with 2 items', () => {
    const wrapper = mount(<CardList data={mockData} />);
    expect(wrapper.find('div.card-list')).toBeDefined();
    expect(wrapper.find('div.card-list').children()).toHaveLength(2);
    expect(wrapper.find(Card)).toHaveLength(2);
  });
});

describe('Card component', () => {
  const wrapper = mount(<Card data={berlinPayload} />);
  it('Should validate Card structure', () => {
    expect(wrapper).toBeDefined();
    expect(wrapper.find('section.card')).toHaveLength(1);
    // header
    expect(wrapper.find('header.card-header')).toHaveLength(1);
    expect(wrapper.find('h1.card-city').text()).toBe('Berlin');
    expect(wrapper.find('time.card-date').text()).toBe('Wed Jan 02 2019');
    // content
    expect(wrapper.find('div.card-content')).toHaveLength(1);
    expect(wrapper.find('span.card-sky').text()).toBe('Clear');
    expect(wrapper.find('h2.card-temp').text()).toBe('-1.5 °C');
    // card-max-min
    expect(wrapper.find('div.card-max-min').children().first().text()).toBe('Max: -1 °C');
    expect(wrapper.find('div.card-max-min').children().last().text()).toBe('Min: -2 °C');
  });
});