import { customFetch } from '../utils';

const URL = 'https://api.openweathermap.org/data/2.5/weather';
const KEY = '?APPID=24312b65699c3bccbcd0fab6eeaf5681';
const QUERY = '&q=';

describe('Verify customFetch method', () => {
  it('Should validate success request', async () => {
    const url = `${URL}${KEY}${QUERY}berlin`
    const result = await customFetch(url);
    expect(result).toBeInstanceOf(Object);
    expect(result.name).toBe('Berlin');
  });

  it('Should validate error request', async () => {
    const url = `${URL}${KEY}${QUERY}xxxxx`
    try {
      await customFetch(url);
    } catch (error) {
      expect(error).toEqual(Error(404));
    }
  });
});
